<?php
/**
 * @file
 * Contains Rules hooks, callbacks and helper functions for Commerce Coupon by Terms.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_couponterms_rules_condition_info() {
  $conditions = array();

  $conditions['commerce_couponterms_order_has_product_with_terms'] = array(
    'label' => t('Order contains a product that shares at least one term with the coupon'),
    'group' => t('Commerce Coupon'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'description' => t('Specifies the order to check.'),
      ),
      'commerce_coupon' => array(
        'type' => 'commerce_coupon',
        'label' => t('Coupon'),
        'description' => t('Specifies the coupon with the product reference field.'),
      ),
      'field' => array(
        'type' => 'text',
        'label' => t('Field'),
        'description' => t('Specifies the taxonomy term reference field that should be compared.'),
        'options list' => 'commerce_couponterms_coupon_get_termref_field_options',
        'restriction' => 'input',
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_couponterms_condition_order_has_product_with_matching_term',
    ),
  );

  $conditions['commerce_couponterms_product_has_terms'] = array(
    'label' => t('Product shares at least one term with the coupon'),
    'group' => t('Commerce Coupon'),
    'parameter' => array(
      'commerce_product' => array(
        'type' => 'commerce_product',
        'label' => t('Product'),
        'description' => t('Specifies the product to check.'),
      ),
      'commerce_coupon' => array(
        'type' => 'commerce_coupon',
        'label' => t('Coupon'),
        'description' => t('Specifies the coupon with the product reference field.'),
      ),
      'field' => array(
        'type' => 'text',
        'label' => t('Field'),
        'description' => t('Specifies the taxonomy term reference field that should be compared.'),
        'options list' => 'commerce_couponterms_coupon_get_termref_field_options',
        'restriction' => 'input',
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_couponterms_condition_product_has_referenced_terms',
    ),
  );

  return $conditions;
}

/**
 * Rule condition callback for checking if an order contains at least one product
 * that shares a term with a coupon.
 * 
 * @return
 *   TRUE if $order contains a product with terms referenced by $field on $commerce_coupon;
 *   FALSE otherwise.
 */
function commerce_couponterms_condition_order_has_product_with_matching_term($order, $commerce_coupon, $field) {
  $coupon_terms = _commerce_couponterms_get_coupon_terms($commerce_coupon, $field);
  $product_term_ref_fields = _commerce_couponterms_get_product_termref_field_names();
  
  // Go through line items looking for a match.
  if ($coupon_terms) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
      // Loop through all of the term reference fields on the product entity
      foreach ($product_term_ref_fields as $field_name) {
        if (isset($line_item_wrapper->commerce_product->{$field_name})) {
          $product_term_values = $line_item_wrapper->commerce_product->{$field_name}->value();
          if (_commerce_couponterms_check_matches($product_term_values, $coupon_terms)) {
            return TRUE;
          }
        }
      }
    }
  }
  
  // Could not find product with a matching term.
  return FALSE;
}

/**
 * Rule condition callback for checking if a product shares a term with a coupon.
 * 
 * @return
 *   TRUE if $product references terms referenced by $field on $commerce_coupon;
 *   FALSE otherwise.
 */
function commerce_couponterms_condition_product_has_referenced_terms($product, $commerce_coupon, $field) {
  $coupon_terms = _commerce_couponterms_get_coupon_terms($commerce_coupon, $field);
  $product_term_ref_fields = _commerce_couponterms_get_product_termref_field_names();
  
  // Go through line items looking for a match.
  if ($coupon_terms) {
    $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
    // Loop through all of the term reference fields on the product entity
    foreach ($product_term_ref_fields as $field_name) {
      if (isset($product_wrapper->{$field_name})) {
        $product_term_values = $product_wrapper->{$field_name}->value();
        if (_commerce_couponterms_check_matches($product_term_values, $coupon_terms)) {
          return TRUE;
        }
      }
    }
  }
  
  // Could not find product with a matching term.
  return FALSE;
}

/**
 * Returns an array of term tids for terms chosen within a term reference field of a coupon
 *
 * @param obj $commerce_coupon
 *   Commerce coupon entity
 * @param $field
 *   Name of a taxonomy term reference field
 *
 * @return mixed
 *   Array of term ids; FALSE if none
 */
function _commerce_couponterms_get_coupon_terms($commerce_coupon, $field) {
  // Build list of referenced term tids
  $coupon_wrapper = entity_metadata_wrapper('commerce_coupon', $commerce_coupon);
  
  if (!isset($coupon_wrapper->{$field})) {
    return FALSE;
  }
  
  $coupon_terms = array();
  foreach ($coupon_wrapper->{$field} as $term) {
    $tid = $term->tid->value();
    $coupon_terms[$tid] = $tid;
  }

  if (count($coupon_terms > 0)) {
    return $coupon_terms;
  }
  else {
    return FALSE;
  }
}

/**
 * Checks if terms within a product term reference field match a coupon's terms
 *
 * @param obj $term_ref_field
 *   Values of a term reference field (from a product)
 * @param array $coupon_terms
 *   Terms set on the coupon entity
 *
 * @return bool
 *   TRUE if match found
 */
function _commerce_couponterms_check_matches($product_term_values, $coupon_terms) {
  if (is_array($product_term_values)) {
    // There are multiple terms on this product for this term reference field.
    // Loop through each value and check if the coupon has a matching term.
    foreach ($product_term_values as $term) {
      if (isset($coupon_terms[$term->tid])) {
        return TRUE;
      }
    }
  }
  else {
    // There is only one term on this product for this term reference field.
    // Check if the coupon has a matching term.
    if (isset($coupon_terms[$product_term_values->tid])) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Option list callback for Rules term reference field options
 *
 * Options list callback (for Rules UI) that returns an array of
 * taxonomy_term_reference fields that are attached to the
 * commerce_coupon entity.
 *
 * @return array
 *   Taxonomy term reference field names
 */
function commerce_couponterms_coupon_get_termref_field_options() {
  $fields = array();
  
  foreach (field_info_fields() as $field) {
    // Check if the field is attached to the commerce_coupon entity.
    if (isset($field['bundles']['commerce_coupon'])) {
      $type = $field['type'];
      $valid_term_reference = ($type == 'taxonomy_term_reference');
      if ($valid_term_reference) {
        $fields[$field['field_name']] = $field['field_name'];
      }
    }
  }
  
  return $fields;
}

/**
 * Returns array of taxonomy_term_reference fields attached to the commerce_product entity.
 *
 * @return array
 *   Taxonomy term reference field names
 */
function _commerce_couponterms_get_product_termref_field_names() {
  $fields = array();
  
  foreach (field_info_fields() as $field) {
    // Check if the field is attached to the commerce_product entity.
    if (isset($field['bundles']['commerce_product'])) {
      $type = $field['type'];
      $valid_term_reference = ($type == 'taxonomy_term_reference');
      if ($valid_term_reference) {
        $fields[$field['field_name']] = $field['field_name'];
      }
    }
  }
  
  return $fields;
}
