##########################
GENERAL
##########################
Note: this module won't do anything for you until you've implemented
the rules described in the "THE RULES" section below.

This module uses Commerce, Commerce Coupon, Commerce Coupon Percentage,
Commerce Coupon Fixed Amount, and Rules to do things like, "Create a 15% off
coupon that applies to any product tagged with XYZ" (where XYZ is a taxonomy
term). It will work with multiple taxonomy terms, and the coupon will work if
ANY of the terms match.

This module provides the rules conditions necessary to allow coupons to be applied
if products in the order have certain taxonomy terms.

The first step is to modify the "Percentage coupon" coupon type (created by
the commerce_coupon_pct module) and/or (depending on which you want)
the Commerce Coupon Fixed Amount (created by the commerce_coupon_fixed_amount
module) to include a (shared) field called "Included product types".
The machine name, which you'll see in the Rules examples below, is
"field_included_product_types." This is a taxonomy term reference field that uses
a "Product Tags" vocabulary. The help text that I use for this field is
something like, "Do not select any terms if you want the coupon to apply
to all products. Otherwise, select as many terms as you'd like. For percentage
coupons, if ANY of the selected terms are found on a product, the product will
receive the discount. For fixed coupons, if ANY of the selected terms are found
on a product in the order, the entire order will get the fixed discount."

I have products in my system that are tagged with terms from the same
vocabulary. The field within the Product type has a machine name of "field_tags."
They're different, but it doesn't matter. The code in this module will provide a
condition to check if "Product shares at least one term with the coupon." On the
coupon side, when making the rules, you do have to choose a specific taxonomy term
reference field, but the coupon terms will be compared against ALL of the taxonomy
term reference field values on the product entities.

Fixed Coupons require just the first rule.
Percentage Coupons require all three (first rule is shared by both).

There is a rule that will check if the order contains ANY products where the
terms match. If the order does NOT contain any, it will tell the system that the coupon
is invalid. This rule is used by both Percentage and Fixed coupons.

The way the rules are defined below, the coupon will be applied to the entire order
if there aren't any taxonomy terms on the coupon. If there is a taxonomy term reference
field (you will pick which term ref field when setting up the rule) with at least one
term selected, the coupon will only work if a product has a matching term. You can see
these conditionals in the "Apply coupon to line item (checking product terms)" rule
component.

##########################
THE RULES
##########################
The following are two triggered rules and one rules component that
must exist for this module to be beneficial to you. I would've
created these programmatically, but the rules must be specific to
the taxonomy term reference field(s) you've created.

This rule is used for both Percentage Coupons and Fixed Coupons

Rule type: Triggered/Reaction rule (normal rule)
Name: Coupon Validation: Check products for matching coupon tags
Event: Validate a coupon
Conditions:
  Entity has field
    Parameter: Entity: [coupon], Field: field_included_product_types
  NOT Data value is empty
    Parameter: Data to check: [coupon:field-included-product-types]
  NOT Order contains a product that shares at least one term with the coupon
    Parameter: Order: [commerce_order], Coupon: [coupon],
    ... Field: field_included_product_types
Actions: Set coupon as invalid

============================================================================

This component rule is only used for Percentage Coupons

Rule type: Component
Name: Apply coupon to line item (checking product terms)
Variables:
  Data Type            Label       Machine Name   Usage
  -----------------------------------------------------------
  Commerce Line Item   Line item   line_item      Parameter
  Commerce Coupon      Coupon      coupon         Parameter
Conditions (as laid out in the Rules UI):
  OR
    AND
      Entity has field
        Parameter: Entity: [coupon], Field: field_included_product_types
      Entity has field
        Parameter: Entity: [line-item], Field: commerce_product
      Product shares at least one term with the coupon
        Parameter: Product: [line-item:commerce-product], Coupon: [coupon],
        ... Field: field_included_product_types
    AND
      Entity has field
        Parameter: Entity: [coupon], Field: field_included_product_types
      Data value is empty
        Parameter: Data to check: [coupon:field-included-product-types]
    NOT Entity has field
      Parameter: Entity: [coupon], Field: field_included_product_types
Actions:
  Apply a percentage coupon to a product line item
    Parameter: Line item: [line_item], Coupon: [coupon],
    ... Price component type: [coupon:price-component-name]

============================================================================

This rule is only used for Percentage Coupons

(Modified rule created by commerce_coupon_pct - simply swapping the default
"Apply a percentage coupon to a product line item" action for our new
"rule: Apply coupon to line item (checking product terms)" component.

Rule type: Triggered/Reaction rule (normal rule)
Name: Apply percentage coupons to product line item
Event: Calculating the sell price of a product
Conditions:
  NOT Data value is empty
    Parameter: Data to check: [commerce-line-item:line-item-id]
  Entity has field
    Parameter: Entity: [commerce-line-item:order],
    ... Field: commerce_coupon_order_reference
Actions:
  Loop
    Parameter: List: [commerce-line-item:order:commerce-coupon-order-reference],
    ... List item: Current coupon (list_coupon)
      rule: Apply coupon to line item (checking product terms)
        Parameter: Line item: [commerce-line-item], Coupon: [list-coupon]

##########################
CREDITS
##########################
My starting point was http://drupal.org/project/commerce_couponprodref
The code was very clean and really got the ball rolling for me. Thanks!